# Basic setup information
install
keyboard us --xlayouts=us --vckeymap=us
lang en_US.UTF-8
rootpw rocky
timezone --isUtc --nontp UTC
selinux --enforcing
firewall --enabled --port=22
network --bootproto=dhcp --device=eth0 --activate --onboot=on
services --enabled=sshd,NetworkManager,chronyd,zram-swap
shutdown
bootloader --location=mbr

# Repositories to use
repo --name="instBaseOS"    --baseurl=http://dl.rockylinux.org/pub/rocky/8/BaseOS/aarch64/os/    --cost=100
repo --name="instAppStream" --baseurl=http://dl.rockylinux.org/pub/rocky/8/AppStream/aarch64/os/ --cost=200
# repo owned by markvnl containing the kernel, zram and aarch64-img-extra-config
repo --name="instKernel"    --baseurl=http://vps01.havak.nl/rocky/8/devel/aarch64/               --cost=300

# Package setup
%packages
@core
NetworkManager-wifi
net-tools
chrony
cloud-utils-growpart
nano
raspberrypi-kernel4
raspberrypi-firmware
zram
aarch64-img-extra-config
-tuned
which
-dracut-config-rescue
-kernel-tools
-iwl*
%end

# Disk setup
clearpart --initlabel --all
part /boot  --fstype=vfat --size=512  --label=boot   --asprimary --ondisk=img
part /      --fstype=ext4 --size=2048 --label=rootfs --asprimary --ondisk=img


%pre
# nothing to do
%end


%post 

# Specific cmdline.txt files needed for raspberrypi
cat > /boot/cmdline.txt << EOF
console=ttyAMA0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline rootwait
EOF


# cpu_governor.service
echo "Applying cpu governor fix..."
cat > /etc/systemd/system/cpu_governor.service << EOF
# FIXME the raspberrypi2-kernel(4) defaults to conservative governor,
# moreover the kernel-tools package is absent is the rpi-kernel repository.

[Unit]
Description=Set cpu governor to ondemand

[Service]
Type=oneshot
ExecStart=/bin/sh -c " for i in {0..3}; do echo ondemand > /sys/devices/system/cpu/cpu\$i/cpufreq/scaling_governor; done"

[Install]
WantedBy=multi-user.target
EOF

systemctl enable cpu_governor.service


# Root README file
cat >/root/README << EOF
== Rocky Linux Community Effort for Raspberypi ==

If you want to automatically resize your / (root)partition, just type the following (as root user):
rootfs-expand

EOF


# Remove ifcfg-link on pre generated images
rm -f /etc/sysconfig/network-scripts/ifcfg-link

# Remove machine-id on pre generated images
rm -f /etc/machine-id
touch /etc/machine-id

# Cleanup yum cache
yum clean all


# add (default disabled) Community Development Repository owned by markvnl
cat > /etc/yum.repos.d/markvnl.repo << EOF
[markvnl]
name= Community Development Repository owned by markvnl
baseurl=http://vps01.havak.nl/rocky/8/devel/aarch64/
skip_if_unavailable=True
gpgcheck=0
repo_gpgcheck=0
enabled=0
EOF


%include ks/RPI-wifi.ks


%end
